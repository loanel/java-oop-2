package agh.edu.machowski;

import java.util.ArrayList;

import java.util.List;

class Employee{
    protected final String firstname;
    protected final String lastname;
    protected int salary;

    public Employee(String firstname, String lastname, int salary) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public String IntroduceYourself(){
        return firstname+" "+lastname;
    }

    public String Happiness(){
        if(salary<10000) return("No :(");
        else return("Yes :)");
    }

}


class ManagerA extends Employee implements Manager{

    protected int budget;
    List<Employee> workers = new ArrayList<Employee>();
    public ManagerA(String firstname, String lastname, int salary, int budget){
        super(firstname, lastname, salary);
        this.budget = budget;
    }
    @Override
    public String CanHire(Employee A){
        if((budget-A.salary)>0) return "Yes";
        else return "No";
    }
    @Override
    public void Hire(Employee A){
        if(CanHire(A)=="Yes"){
            budget -= A.salary;
            workers.add(A);
        }
    }
}
class ManagerB extends Employee implements Manager{
    protected int amount;
    List<Employee> workers = new ArrayList<Employee>();
    public ManagerB(String firstname, String lastname, int salary, int amount){
        super(firstname, lastname, salary);
        this.amount = amount;
    }
    @Override
    public String CanHire(Employee A){
        if(amount>0) return "Yes";
        else return "No";
    }
    @Override
    public void Hire(Employee A){
        if(CanHire(A)=="Yes"){
            amount-=1;
            workers.add(A);
        }
    }
}

class Company{
    private static Company instance = null;
    ManagerB CEO;
    protected Company(){};

    public static Company getInstance(){
        if(instance == null){
            instance = new Company();
        }
        return instance;
    }
    public void HireCeo(String firstname, String lastname, int salary, int budget ){
        CEO = new ManagerB(firstname, lastname, salary, budget);
    } /// przyjalem ze CEO to manager ktory ma budzet na zatrudnienie reszty pracownikow
    @Override
    public String toString(){
        Employee pom;
        String napis = CEO.IntroduceYourself()+" - CEO\n";
        for(int i=0; i<CEO.workers.size(); i++) {
            napis += "    " + CEO.workers.get(i).IntroduceYourself();
            pom = CEO.workers.get(i);
            if (pom instanceof ManagerA) {
                napis += " - Manager\n";
                for (Employee k : ((ManagerA) pom).workers) {
                    napis += "        " + k.IntroduceYourself() + " - Employee\n";
                }
            }
            else if (pom instanceof ManagerB) {
                napis += " - Manager\n";
                for (Employee k : ((ManagerB) pom).workers) {
                    napis += "        " + k.IntroduceYourself() + " - Employee\n";
                }
            }
            else napis += " - Employee\n";
        }
        return napis;
    }
}
public class App 
{
    public static void main( String[] args )
    {
        Company Firma= Company.getInstance();
        Firma.HireCeo("Mateusz", "Machowski", 12000, 1000000);
        System.out.print(Firma.CEO.IntroduceYourself());
    }
}
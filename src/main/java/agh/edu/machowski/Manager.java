package agh.edu.machowski;

public interface Manager{
    public String CanHire(Employee A);
    public void Hire(Employee A);
}

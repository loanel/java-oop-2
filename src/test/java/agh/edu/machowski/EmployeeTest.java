package agh.edu.machowski;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by loanel on 09.04.16.
 */
public class EmployeeTest {
    Employee worker;
    @Before
    public void setUp() throws Exception {
        worker = new Employee("Jan", "Abacki", 11000);
    }

    @Test
    public void PrintName(){
        assertEquals("Jan Abacki", worker.IntroduceYourself());
    }
    public void TestHappines(){
        assertEquals("Yes :)", worker.Happiness());
    }

}
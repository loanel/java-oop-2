package agh.edu.machowski;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by loanel on 09.04.16.
 */
public class ManagerBTest {
    Employee worker;
    Employee worker2;
    ManagerB manager;
    @Before
    public void setUp() throws Exception{
        worker = new Employee("Jan", "Abacki", 11000);
        manager = new ManagerB ("Tomek", "Nowak", 11000, 30000);
        worker2 = new Employee("Ada", "Abacka", 15000);
    }
    @Test
    public void CanHeHire(){
        assertEquals("Yes", manager.CanHire(worker));
    }
    public void Hiring(){
        manager.Hire(worker);
        Employee test = manager.workers.get(0);
        assertEquals("Jan Abacki", test.IntroduceYourself() );
        manager.Hire(worker2);
        test = manager.workers.get(1);
        assertEquals("Ada ", "Abacka", test.IntroduceYourself() );
    }
}
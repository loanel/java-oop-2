package agh.edu.machowski;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by loanel on 09.04.16.
 */
public class CompanyTest {
    Employee tester;
    Company Firma= Company.getInstance();
    @Before
    public void setUp() throws Exception{
        tester=new ManagerA("Jan", "Abacki" , 10000, 100000);
        Firma.HireCeo("Mateusz", "Machowski", 12000, 1000000);
        Firma.CEO.Hire(tester);
    }
    @Test
    public void testtoObject(){
        assertEquals("Mateusz Machowski - CEO\n    Jan Abacki - Manager\n", Firma.toString());
    }
}
package agh.edu.machowski;

import org.junit.Before;
import org.junit.Test;
import java.util.LinkedList;
import static org.junit.Assert.*;

/**
 * Created by loanel on 09.04.16.
 */
public class ManagerATest {
    Employee worker;
    ManagerA manager;
    @Before
    public void setUp() throws Exception{
        worker = new Employee("Jan", "Abacki", 11000);
        manager = new ManagerA ("Tomek", "Nowak", 11000, 30000);
    }
    @Test
    public void CanHeHire(){
        assertEquals("Yes", manager.CanHire(worker));
    }
    public void Hiring(){
        manager.Hire(worker);
        Employee test = manager.workers.get(0);
        assertEquals("Jan Abacki", test.IntroduceYourself() );
    }
}